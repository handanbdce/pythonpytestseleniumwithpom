import time

from Tests.test_base import BaseTest

class TestHomePage(BaseTest):

    """This case adds 3 items to cart"""
    def test_addtocart(self):
        assert self.homePage.get_products_title() == "PRODUCTS"
        self.homePage.add_backpack_tocart()
        self.homePage.add_light_tocart()
        self.homePage.add_tshirt_tocart()
        assert self.header.get_cart_items() == "3"

    """This case adds 2 items and remove 1 items from cart"""
    def test_removefromhomepage(self):
        assert self.homePage.get_products_title() == "PRODUCTS"
        self.homePage.add_backpack_tocart()
        self.homePage.add_light_tocart()
        assert self.header.get_cart_items() == "2"
        self.homePage.remove_backpack_fromcart()
        assert self.header.get_cart_items() == "1"
        time.sleep(10)
